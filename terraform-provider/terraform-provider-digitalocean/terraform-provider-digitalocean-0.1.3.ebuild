# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=6

inherit eutils portability toolchain-funcs unpacker user

AMD64_LINUX="${PN}_${PV}_linux_amd64"
X86_LINUX="${PN}_${PV}_linux_386"

#X86_FBSD="${PN}_${PV}_freebsd_386"
#AMD64_FBSD="${PV}_${PN}_${PV}_freebsd_amd64"

#ARM_LINUX="${PN}_${PV}_linux_arm"
#ARM64_LINUX="${PN}_${PV}_linux_arm64"

HC_URI="https://releases.hashicorp.com/"

SRC_URI="
    amd64? ( ${HC_URI}${PN}/${PV}/${AMD64_LINUX}.zip )
    x86? ( ${HC_URI}${PN}/${PV}/${X86_LINUX}.zip )
"
#    amd64-fbsd? ( ${HC_URI}${PN}/${PV}/${AMD64_FBSD}.zip )
#    x86-fbsd? ( ${HC_URI}${PN}/${PV}/${X86_FBSD}.zip )
#    arm? ( ${HC_URI}${PN}/${PV}/${ARM_LINUX}.zip )
#    arm64? ( ${HC_URI}${PN}/${PV}/${ARM64_LINUX}.zip )
#"

LICENSE="MPL-2.0"
SLOT="0"

#KEYWORDS="-* amd64 x86 amd64-fbsd x86-fbsd"
KEYWORDS="-* amd64 x86"
RESTRICT="bindist mirror"

S="${WORKDIR}"

src_prepare() {
        default
}

src_install() {
        default
	insinto /usr/bin
	fperms +x .
        doins -r .
}

