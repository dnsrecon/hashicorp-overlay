# Setup 
Easy

## Install layman
`emerge layman`

## Add overlay xml file
`/etc/layman/overlays/hashicorp-overlay.xml:`

```
<repositories version="1.0">
        <repo priority="50" quality="experimental" status="unofficial">
                <name>hashicorp-overlay</name>
                <description> Gentoo Overlay for bindist Hashicorp Releases</description>
                <homepage>http://www.github.com/netcrave/hashicorp-overlay</homepage>
                <owner>
                        <email>paigeadele@gmail.com</email>
                </owner>
                <source type="git">git://github.com/netcrave/hashicorp-overlay.git</source>
        </repo>
</repositories>
```

## Sync layman
`layman -f && layman -S`

## Add layman repo
`layman -a hashicorp-overlay`
