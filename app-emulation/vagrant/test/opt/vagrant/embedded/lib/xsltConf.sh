#
# Configuration file for using the xslt library
#
XSLT_LIBDIR="-L/tmp/vagrant-temp/embedded/lib"
XSLT_LIBS="-lxslt  -L/tmp/vagrant-temp/embedded/lib -lxml2 -liconv -lm -ldl -lm -lrt"
XSLT_INCLUDEDIR="-I/tmp/vagrant-temp/embedded/include"
MODULE_VERSION="xslt-1.1.28"
