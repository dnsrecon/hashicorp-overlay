#
# Configuration file for using the XML library in GNOME applications
#
XML2_LIBDIR="-L/tmp/vagrant-temp/embedded/lib"
XML2_LIBS="-lxml2    -liconv -lm "
XML2_INCLUDEDIR="-I/tmp/vagrant-temp/embedded/include/libxml2"
MODULE_VERSION="xml2-2.9.1"

