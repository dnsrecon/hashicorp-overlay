Gem::Specification.new do |s|
  s.name = "minitest"
  s.version = "4.3.2"
  s.summary = "This minitest is bundled with Ruby"
  s.executables = []
  s.files = ["minitest/pride.rb", "minitest/autorun.rb", "minitest/parallel_each.rb", "minitest/benchmark.rb", "minitest/unit.rb", "minitest/mock.rb", "minitest/spec.rb", "minitest/hell.rb"]
end
